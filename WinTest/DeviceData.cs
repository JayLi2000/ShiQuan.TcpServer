﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinTest
{
    /// <summary>
    /// 设备数据
    /// </summary>
    public class DeviceData
    {
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime datetime { get; set; }
        /// <summary>
        /// 设备标识
        /// </summary>
        public string serialnumber { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int index { get; set; }
        /// <summary>
        /// pm10
        /// </summary>
        public string data01 { get; set; }
        /// <summary>
        /// pm25
        /// </summary>
        public string data02 { get; set; }
        /// <summary>
        /// 噪声
        /// </summary>
        public string data03 { get; set; }
        /// <summary>
        /// 温度
        /// </summary>
        public string data04 { get; set; }
        /// <summary>
        /// 湿度
        /// </summary>
        public string data05 { get; set; }
        /// <summary>
        /// 风向
        /// </summary>
        public string data06 { get; set; }
        /// <summary>
        /// 风速
        /// </summary>
        public string data07 { get; set; }
        /// <summary>
        /// 温度
        /// </summary>
        public string data08 { get; set; }
        /// <summary>
        /// 湿度
        /// </summary>
        public string data09 { get; set; }

    }
}
