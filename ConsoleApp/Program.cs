﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //构造一个TcpClient类对象,TCP客户端
            TcpClient client = new TcpClient();
            try
            {
                Console.WriteLine("按任意键继续...");
                Console.ReadLine();

                Console.WriteLine(DateTime.Now.ToString());
                Console.WriteLine("\n开始连接服务器...");
                //与TCP服务器连接
                client.Connect("127.0.0.1", 8080);

                //创建网络流,获取数据流
                NetworkStream stream = client.GetStream();
                //写数据流对象
                StreamWriter sw = new StreamWriter(stream);
                Console.WriteLine("\n发送请求...");
                sw.WriteLine("获取报告内容...");
                sw.Flush();

                //读数据流对象
                stream = client.GetStream();
                //StreamReader sr = new StreamReader(stream);
                //var content = sr.ReadToEnd();
                //Console.WriteLine("\n返回结果：" + content.Trim());

                byte[] result = new byte[1024];
                var recCount = stream.Read(result, 0, result.Length);
                if (recCount > 0)
                {
                    var content = System.Text.Encoding.UTF8.GetString(result,0,recCount);
                    Console.WriteLine("\n返回结果：" + content.Trim('\n'));
                    if(content == "接收成功！")
                        Console.WriteLine("\n接收成功");
                }
                else
                    Console.WriteLine("\n返回结果为空！");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n请求异常：" + ex.ToString());
            }
            finally
            {
                client.Close();
            }
            ////定义发送数据缓存
            //byte[] data = new byte[1024];
            ////定义主机的IP及端口
            //IPAddress ip = IPAddress.Parse("127.0.0.1");
            //IPEndPoint ipEnd = new IPEndPoint(ip, 8080);
            ////定义套接字类型
            //Socket socket = null;
            //int recv = 0;
            //try
            //{
            //    socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //    //尝试连接
            //    try
            //    {
            //        socket.ReceiveTimeout = 30 * 1000;
            //        socket.Connect(ipEnd);
            //        //定义接收数据的长度
            //        recv = socket.Receive(data);
            //        if (recv > 0)
            //        {
            //            //将接收的数据转换成字符串
            //            var result = Encoding.UTF8.GetString(data, 0, recv);
            //            //控制台输出接收到的数据
            //            Console.WriteLine(result);
            //        }
            //    }
            //    //异常处理
            //    catch (SocketException e)
            //    {
            //        Console.Write("Fail to connect server");
            //        Console.Write(e.ToString());
            //        return;
            //    }
            //    //将从键盘获取的字符串转换成整型数据并存储在数组中    
            //    data = Encoding.UTF8.GetBytes("获取报告内容...");
            //    //发送该数组
            //    socket.Send(data, data.Length, SocketFlags.None);
            //    using (MemoryStream memory = new MemoryStream())
            //    {
            //        do
            //        {
            //            //对data清零
            //            data = new byte[10];
            //            //定义接收到的数据的长度
            //            recv = socket.Receive(data);
            //            Console.WriteLine("recv=" + recv);
            //            if (recv > 0)
            //            {
            //                memory.Write(data, 0, recv);
            //            }

            //        } while (recv >= 10);
            //        data = memory.ToArray();
            //        Console.WriteLine(System.Text.Encoding.UTF8.GetString(data, 0, data.Length));
            //    }
            //    //对data清零
            //    //data = new byte[1024];
            //    //定义接收到的数据的长度
            //    //recv = socket.Receive(data);
            //    //Console.WriteLine("recv=" + recv);
            //    //if (recv > 0)
            //    //{
            //    //    Console.WriteLine(System.Text.Encoding.UTF8.GetString(data, 0, recv));
            //    //}

            //    Console.Write("disconnect from server");
            //}
            //catch (Exception ex)
            //{
            //    Console.Write("读取异常：" + ex.ToString());
            //}
            //finally
            //{
            //    if(socket != null)
            //    {
            //        socket.Shutdown(SocketShutdown.Both);
            //        socket.Close();
            //    }
            //}
            Console.ReadLine();
        }
    }
}
