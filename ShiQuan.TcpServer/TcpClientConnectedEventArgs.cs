﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.TcpServer
{
    /// <summary>
    /// 客户端连接事件
    /// </summary>
    public class TcpClientConnectedEventArgs : EventArgs
    {
        /// <summary>
        /// 获取客户端
        /// </summary>
        public TcpClient TcpClient { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tcp"></param>
        public TcpClientConnectedEventArgs(TcpClient tcp)
        {
            TcpClient = tcp;
        }
    }
}
