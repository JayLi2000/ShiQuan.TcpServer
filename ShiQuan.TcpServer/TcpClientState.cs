﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.TcpServer
{
    /// <summary>
    /// Tcp 客户端
    /// </summary>
    public class TcpClientState
    {
        /// <summary>
        /// 获取TcpClient
        /// </summary>
        public TcpClient TcpClient { get; private set; }
        /// <summary>
        /// 响应缓存
        /// </summary>
        public byte[] Buffer { get; private set; }
        /// <summary>
        /// 网络数据流
        /// </summary>
        public NetworkStream Stream { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <param name="buffer"></param>
        public TcpClientState(TcpClient tcpClient, byte[] buffer)
        {
            this.TcpClient = tcpClient;

            this.Buffer = buffer;

            this.Stream = tcpClient.GetStream();
        }
        /// <summary>
        /// 关闭套接字连接并允许重用套接字。
        /// </summary>
        /// <param name="reuseSocket"></param>
        public void Disconnect(bool reuseSocket)
        {
            this.TcpClient.Client.Disconnect(reuseSocket);
        }
    }
}
