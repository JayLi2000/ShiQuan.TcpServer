﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.TcpServer
{
    /// <summary>
    /// 客户端响应事件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TcpClientReceivedEventArgs<T> : EventArgs
    {
        /// <summary>
        /// 客户端
        /// </summary>
        public TcpClient TcpClient { private set; get; }
        /// <summary>
        /// 获取数据响应数据
        /// </summary>
        public T Datagram { private set; get; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <param name="datagram"></param>
        public TcpClientReceivedEventArgs(TcpClient tcpClient, T datagram)
        {

            this.TcpClient = tcpClient;

            this.Datagram = datagram;
        }

    }
}
