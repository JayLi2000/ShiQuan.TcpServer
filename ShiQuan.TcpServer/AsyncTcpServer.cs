﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ShiQuan.TcpServer
{
    /// <summary>
    /// 异步TCP服务器
    /// </summary>
    public class AsyncTcpServer : IDisposable
    {
        #region Fields
        /// <summary>
        /// 侦听来自 TCP 网络客户端的连接。
        /// </summary>
        private TcpListener listener;
        private bool listenState = false;
        /// <summary>
        /// TCP 网络客户端
        /// </summary>
        private List<TcpClientState> clients;
        /// <summary>
        /// 是否释放资源
        /// </summary>
        private bool disposed = false;

        #endregion

        #region Properties
        
        /// <summary>
        /// 监听的IP地址
        /// </summary>
        public IPAddress Address { get; private set; }
        /// <summary>
        /// 监听的端口
        /// </summary>
        public int Port { get; private set; }
        /// <summary>
        /// 通信使用的编码
        /// </summary>
        public Encoding Encoding { get; set; }

        #endregion

        #region Ctors

        /// <summary>
        /// 异步TCP服务器
        /// </summary>
        /// <param name="listenPort">监听的端口</param>
        public AsyncTcpServer(int listenPort)
          : this(IPAddress.Any, listenPort)
        {
        }

        /// <summary>
        /// 异步TCP服务器
        /// </summary>
        /// <param name="localEP">监听的终结点</param>
        public AsyncTcpServer(IPEndPoint localEP)
          : this(localEP.Address, localEP.Port)
        {
        }

        /// <summary>
        /// 异步TCP服务器
        /// </summary>
        /// <param name="localIPAddress">监听的IP地址</param>
        /// <param name="listenPort">监听的端口</param>
        public AsyncTcpServer(IPAddress localIPAddress, int listenPort)
        {
            this.Address = localIPAddress;
            this.Port = listenPort;
            this.Encoding = Encoding.Default;

            this.clients = new List<TcpClientState>();

            this.listener = new TcpListener(Address, Port);
            this.listener.AllowNatTraversal(true);
        }

        #endregion
        
        #region Server

        /// <summary>
        /// 启动服务器
        /// </summary>
        /// <returns>异步TCP服务器</returns>
        public AsyncTcpServer Start()
        {
            if (this.listenState == false)
            {
                this.listener.Start();
                this.listenState = true;

                listener.BeginAcceptTcpClient(
                  new AsyncCallback(AsyncAcceptResult), listener);
            }
            return this;
        }

        /// <summary>
        /// 启动服务器
        /// </summary>
        /// <param name="backlog">
        /// 服务器所允许的挂起连接序列的最大长度
        /// </param>
        /// <returns>异步TCP服务器</returns>
        public AsyncTcpServer Start(int backlog)
        {
            if (this.listenState == false)
            {
                this.listener.Start(backlog);
                this.listenState = true;

                listener.BeginAcceptTcpClient(new AsyncCallback(AsyncAcceptResult), listener);
            }
            return this;
        }

        /// <summary>
        /// 停止服务器
        /// </summary>
        /// <returns>异步TCP服务器</returns>
        public AsyncTcpServer Stop()
        {
            if (this.listenState == false)
                return this;

            listener.Stop();
            listenState = false;

            lock (this.clients)
            {
                for (int i = 0; i < this.clients.Count; i++)
                {
                    this.clients[i].Disconnect(false);
                }
                this.clients.Clear();
            }
            return this;
        }

        #endregion

        #region Receive

        private void AsyncAcceptResult(IAsyncResult ar)
        {
            if (this.listenState == false)
                return;

            TcpListener tcpListener = (TcpListener)ar.AsyncState;

            TcpClient tcpClient = tcpListener.EndAcceptTcpClient(ar);
            byte[] buffer = new byte[tcpClient.ReceiveBufferSize];

            TcpClientState internalClient = new TcpClientState(tcpClient, buffer);
            lock (this.clients)
            {
                this.clients.Add(internalClient);
                OnClientConnected(tcpClient);
            }

            NetworkStream networkStream = internalClient.Stream;
            networkStream.BeginRead(internalClient.Buffer, 0,
                internalClient.Buffer.Length,
                AsyncReadResult,
                internalClient);

            tcpListener.BeginAcceptTcpClient(
              new AsyncCallback(AsyncAcceptResult), ar.AsyncState);
        }

        private void AsyncReadResult(IAsyncResult ar)
        {
            if (this.listenState == false)
                return;

            TcpClientState internalClient = (TcpClientState)ar.AsyncState;
            NetworkStream networkStream = internalClient.Stream;

            int numberOfReadBytes = 0;
            try
            {
                numberOfReadBytes = networkStream.EndRead(ar);
            }
            catch
            {
                numberOfReadBytes = 0;
            }

            if (numberOfReadBytes == 0)
            {
                // connection has been closed
                lock (this.clients)
                {
                    this.clients.Remove(internalClient);
                    OnClientDisconnected(internalClient.TcpClient);
                    return;
                }
            }

            // received byte and trigger event notification
            byte[] receivedBytes = new byte[numberOfReadBytes];
            Buffer.BlockCopy(internalClient.Buffer, 0, receivedBytes, 0, numberOfReadBytes);

            OnClientReceived(internalClient.TcpClient, receivedBytes);
            OnPlaintextReceived(internalClient.TcpClient, receivedBytes);

            // continue listening for tcp datagram packets
            networkStream.BeginRead(internalClient.Buffer, 0,
                internalClient.Buffer.Length,
                AsyncReadResult,
                internalClient);
        }

        #endregion

        #region Events

        /// <summary>
        /// 与客户端的连接已建立事件
        /// </summary>
        public event EventHandler<TcpClientConnectedEventArgs> ClientConnected;

        private void OnClientConnected(TcpClient tcpClient)
        {
            if (ClientConnected != null)
            {
                ClientConnected(this, new TcpClientConnectedEventArgs(tcpClient));
            }
        }

        /// <summary>
        /// 与客户端的连接已断开事件
        /// </summary>
        public event EventHandler<TcpClientDisconnectedEventArgs> ClientDisconnected;
        
        private void OnClientDisconnected(TcpClient tcpClient)
        {
            if (ClientDisconnected != null)
            {
                ClientDisconnected(this, new TcpClientDisconnectedEventArgs(tcpClient));
            }
        }

        /// <summary>
        /// 接收到数据报文事件
        /// </summary>
        public event EventHandler<TcpClientReceivedEventArgs<byte[]>> ClientReceived;
        

        private void OnClientReceived(TcpClient sender, byte[] datagram)
        {
            if (ClientReceived != null)
            {
                ClientReceived(this, new TcpClientReceivedEventArgs<byte[]>(sender, datagram));
            }
        }
        /// <summary>
        /// 接收到数据报文明文事件
        /// </summary>
        public event EventHandler<TcpClientReceivedEventArgs<string>> PlaintextReceived;
        private void OnPlaintextReceived(TcpClient sender, byte[] datagram)
        {
            if (PlaintextReceived != null)
            {
                PlaintextReceived(this, new TcpClientReceivedEventArgs<string>(
                  sender, this.Encoding.GetString(datagram, 0, datagram.Length)));
            }
        }
        #endregion

        #region Send

        /// <summary>
        /// 发送报文至指定的客户端
        /// </summary>
        /// <param name="tcpClient">客户端</param>
        /// <param name="datagram">报文</param>
        public void Send(TcpClient tcpClient, byte[] datagram)
        {
            if (this.listenState == false)
                throw new InvalidProgramException("This TCP server has not been started.");

            if (tcpClient == null)
                throw new ArgumentNullException("tcpClient");

            if (datagram == null)
                throw new ArgumentNullException("datagram");

            tcpClient.GetStream().BeginWrite(
              datagram, 0, datagram.Length, AsyncWriteResult, tcpClient);
        }

        private void AsyncWriteResult(IAsyncResult ar)
        {
            TcpClient tcpClient = ((TcpClient)ar.AsyncState);
            NetworkStream stream = tcpClient.GetStream();
            stream.EndWrite(ar);
        }

        /// <summary>
        /// 发送报文至指定的客户端
        /// </summary>
        /// <param name="tcpClient">客户端</param>
        /// <param name="datagram">报文</param>
        public void Send(TcpClient tcpClient, string datagram)
        {
            Send(tcpClient, this.Encoding.GetBytes(datagram));
        }

        /// <summary>
        /// 发送报文至所有客户端
        /// </summary>
        /// <param name="datagram">报文</param>
        public void SendAll(byte[] datagram)
        {
            if (this.listenState == false)
                throw new InvalidProgramException("This TCP server has not been started.");

            for (int i = 0; i < this.clients.Count; i++)
            {
                Send(this.clients[i].TcpClient, datagram);
            }
        }

        /// <summary>
        /// 发送报文至所有客户端
        /// </summary>
        /// <param name="datagram">报文</param>
        public void SendAll(string datagram)
        {
            if (this.listenState == false)
                throw new InvalidProgramException("This TCP server has not been started.");

            SendAll(this.Encoding.GetBytes(datagram));
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, 
        /// releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            OnDispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release 
        /// both managed and unmanaged resources; <c>false</c> 
        /// to release only unmanaged resources.</param>
        protected virtual void OnDispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    try
                    {
                        Stop();

                        if (listener != null)
                        {
                            listener = null;
                        }
                    }
                    catch (SocketException ex)
                    {
                        throw ex;
                    }
                }

                disposed = true;
            }
        }

        #endregion
    }
}
