﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.HttpServer
{
    /// <summary>
    /// 客户端连接事件
    /// </summary>
    public class SocketConnectedEventArgs : EventArgs
    {
        /// <summary>
        /// 获取客户端
        /// </summary>
        public Socket Socket { get; private set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tcp"></param>
        public SocketConnectedEventArgs(Socket client)
        {
            Socket = client;
        }
    }

}
